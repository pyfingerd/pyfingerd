#!/usr/bin/env python3
# *****************************************************************************
# Copyright (C) 2017-2025 Thomas Touhey <thomas@touhey.fr>
# This file is part of the pyfingerd project, which is MIT-licensed.
# *****************************************************************************
"""This file defines the exceptions used throughout the module."""

from __future__ import annotations


__all__ = ["MalformedRequestError"]


class MalformedRequestError(Exception):
    """Raised when a malformed query is received.

    :param msg: The precise message.
    :param line: The malformed query line.
    """

    __slots__ = ("line", "msg")

    def __init__(
        self,
        msg: str,
        /,
        *,
        line: str,
    ) -> None:
        self.line = line
        self.msg = msg

        super().__init__(
            msg + (f": {line!r}" if line else "") + ".",
        )
