#!/usr/bin/env python3
# *****************************************************************************
# Copyright (C) 2017-2025 Thomas Touhey <thomas@touhey.fr>
# This file is part of the pyfingerd project, which is MIT-licensed.
# *****************************************************************************
"""Finger server."""

from __future__ import annotations

from abc import ABC, abstractmethod
import asyncio
from collections.abc import Callable, Coroutine, Iterable, Iterator
from datetime import datetime
from errno import errorcode
from ipaddress import IPv4Address, IPv6Address
from logging import getLogger
from multiprocessing import Process
import re
from signal import pause
from socket import (
    AF_INET,
    AF_INET6,
    IPPROTO_TCP,
    SOCK_STREAM,
    gaierror,
    getaddrinfo,
)
from traceback import format_exc
from typing import Annotated, Any, TypeVar

from croniter import croniter
from pydantic import (
    AfterValidator,
    BaseModel,
    ConfigDict,
    Field,
    PrivateAttr,
    StringConstraints,
    TypeAdapter,
    field_validator,
)

from .core import FingerFormatter, FingerInterface
from .errors import MalformedRequestError
from .utils import access_logger, decode_finger_text, error_logger


__all__ = ["FingerBaseServer", "FingerRequest", "FingerServer"]

FingerRequestType = TypeVar("FingerRequestType", bound="FingerRequest")
FingerServerBindType = TypeVar(
    "FingerServerBindType",
    bound="FingerServerBind",
)
FingerHostname = Annotated[
    str,
    StringConstraints(
        pattern=r"^[A-Za-z0-9][A-Za-z0-9-]*(\.[A-Za-z0-9][A-Za-z0-9-]*)*$",
    ),
]

BIND_PATTERN = re.compile(
    r"^(?:([a-z]+(?:-[a-z]+)*):)?(?:\[([^\[\]]+)\]|([^\[\]]+))"
    + r"(?::([0-9]+))$",
)
IPV4_PATTERN = re.compile(
    r"^0*(?:25[0-5]|2[0-4][0-9]|1?[0-9][0-9]|[0-9])"
    + r"(\.0*(?:25[0-5]|2[0-4][0-9]|1?[0-9][0-9]|[0-9])){1,3}$",
)

logger = getLogger(__name__)


class FingerRequest(BaseModel):
    """A finger query."""

    model_config = ConfigDict(strict=True, frozen=True)
    """Configuration dictionary for the model."""

    host: str | None = None
    """Host of the query, if the client wants the server to transmit it."""

    query: str | None = None
    """Username query, if provided by the client."""

    verbose: bool = False
    """Whether the client has provided the '/W' or not."""

    line: str
    """The raw query line provided by the client."""

    @classmethod
    def decode(
        cls: type[FingerRequestType],
        raw: bytes,
        /,
    ) -> FingerRequestType:
        """Decode a raw finger query.

        There are three types of requests recognized by RFC 1288:

        * {C} is a request for a list of all online users.
        * {Q1} is a request for a local user.
        * {Q2} is a request for a distant user (with hostname).

        /W means the RUIP (program answering the query) should be more
        verbose (this token can be ignored).

        :param line: The line to decode.
        :return: The query.
        """
        # Get a character string out of the query.
        line = decode_finger_text(raw)

        # Get elements.
        host = None
        username = None
        verbose = False
        for element in line.split():
            if element[0] == "/":
                if not element[1:]:
                    raise MalformedRequestError(
                        "Missing feature flags after '/'",
                        line=line,
                    )

                for letter in element[1:]:
                    if letter == "W":
                        verbose = True
                    else:
                        raise MalformedRequestError(
                            f"Unknown feature flag {letter!r}",
                            line=line,
                        )

                continue

            if username is not None:
                raise MalformedRequestError(
                    "Multiple query arguments",
                    line=line,
                )

            username = element

        if username is not None and "@" in username:
            host, *username_parts = username.split("@")[::-1]
            username = "@".join(username_parts[::-1])

        return cls(host=host, query=username, verbose=verbose, line=line)


class FingerServerBind(BaseModel):
    """Server bind."""

    address: IPv4Address | IPv6Address
    """Address on which to bind the server."""

    port: Annotated[int, Field(ge=0, lt=65536)]
    """Port on which to bind the server."""

    @classmethod
    def decode(
        cls: type[FingerServerBindType],
        raw: str,
        /,
    ) -> list[FingerServerBindType]:
        """Decode server binds from the given formatted hosts, as a list.

        :param raw: Raw binds to decode.
        :return: Binds.
        """
        return [*cls._decode_iter(raw)]

    @classmethod
    def _decode_iter(
        cls: type[FingerServerBindType],
        raw: str,
        /,
    ) -> Iterator[FingerServerBindType]:
        """Decode server binds from the given formatted hosts.

        :param raw: Raw binds to decode.
        :return: Binds.
        """
        for raw_bind in map(str.strip, raw.split(",")):
            if not raw_bind:
                continue

            match = BIND_PATTERN.match(raw_bind)
            if match is None:
                raise ValueError(f"Invalid bind: {raw_bind!r}")

            scheme = match[1]
            if scheme is not None and scheme.casefold() != "finger":
                raise ValueError(
                    f"Unsupported scheme {scheme!r} for bind: {raw_bind!r}",
                )

            port = 79
            if match[4] is not None:
                port = int(match[4])
                if port >= 65536:
                    raise ValueError(
                        f"Invalid port {port} for bind: {raw_bind!r}",
                    )

            if match[2] is not None:
                yield cls(
                    address=IPv6Address(match[2]),
                    port=port,
                )
                continue

            addr_match = IPV4_PATTERN.match(match[3])
            if addr_match is not None:
                ipv4_parts = [*map(int, match[3].split("."))]
                if len(ipv4_parts) == 2:
                    ipv4_parts = [
                        ipv4_parts[0],
                        (ipv4_parts[1] >> 16) & 255,
                        (ipv4_parts[1] >> 8) & 255,
                        ipv4_parts[1] & 255,
                    ]
                elif len(ipv4_parts) == 3:
                    ipv4_parts = [
                        ipv4_parts[0],
                        ipv4_parts[1],
                        (ipv4_parts[2] >> 8) & 255,
                        ipv4_parts[2] & 255,
                    ]

                yield cls(
                    address=IPv4Address(
                        f"{ipv4_parts[0]}.{ipv4_parts[1]}"
                        + f".{ipv4_parts[2]}.{ipv4_parts[3]}",
                    ),
                    port=port,
                )
                continue

            try:
                gai_results = getaddrinfo(
                    match[3],
                    port,
                    proto=IPPROTO_TCP,
                    type=SOCK_STREAM,
                )
            except gaierror as exc:
                raise ValueError(
                    "Invalid IP address and unresolved host: "
                    + f"{match[3]!r}",
                ) from exc

            for family, socktype, _, _, sockaddr in gai_results:
                if socktype != SOCK_STREAM:
                    continue

                if family == AF_INET:
                    yield cls(address=IPv4Address(sockaddr[0]), port=port)
                elif family == AF_INET6:
                    yield cls(
                        address=IPv6Address(sockaddr[0]),
                        port=port,
                    )


class FingerBaseServer(BaseModel, ABC):
    """Base finger server class."""

    model_config = ConfigDict(extra="forbid", arbitrary_types_allowed=True)
    """Model configuration."""

    binds: Annotated[
        list[FingerServerBind],
        Field(min_length=1),
    ] = FingerServerBind.decode("localhost:79")
    """Binds on which the server should listen and answer requests."""

    debug: bool = False
    """Whether tracebacks should be transmitted back to the client or not."""

    _process: Annotated[Process | None, PrivateAttr] = None
    """Process, if the server was created in a different process."""

    @field_validator("binds", mode="before")
    @classmethod
    def _decode_binds(cls, value: Any) -> Any:
        """Decode the binds, if provided as a string.

        :param value: Value to parse if necessary.
        :return: Parsed value.
        """
        if isinstance(value, str):
            return [*FingerServerBind.decode(value)]

        return value

    @abstractmethod
    async def handle_malformed_request(
        self,
        /,
        *,
        exc: MalformedRequestError,
        src: IPv4Address | IPv6Address,
    ) -> str:
        """Handle an invalid request.

        :param exc: Raised exception describing the request parsing error.
        :param src: Source from which the request has been emitted.
        :return: ASCII-compatible result.
        """

    @abstractmethod
    async def handle_request(
        self,
        request: FingerRequest,
        /,
        *,
        src: IPv4Address | IPv6Address,
    ) -> str:
        """Handle a valid request.

        :param request: Decoded request.
        :param src: Source from which the request has been emitted.
        :return: ASCII-compatible result.
        """

    async def _handle_async_finger_connection(
        self,
        inp: asyncio.StreamReader,
        outp: asyncio.StreamWriter,
        /,
    ) -> None:
        """Handle an asynchronous connection.

        :param inp: The stream reader for the connection.
        :param outp: The stream writer for the connection.
        """
        raw_src, *_ = outp.get_extra_info("peername")
        src = TypeAdapter(IPv4Address | IPv6Address).validate_python(raw_src)

        # Gather the request line.
        try:
            line = await inp.readline()
        except ConnectionResetError:
            error_logger.info(
                "%s submitted no request. (possible scan)",
                src,
            )
            return

        try:
            try:
                request = FingerRequest.decode(line)
            except MalformedRequestError as exc:
                response_text = await self.handle_malformed_request(
                    exc=exc,
                    src=src,
                )
            else:
                response_text = await self.handle_request(request, src=src)
        except Exception:
            logger.exception("An error has occurred within the server.")

            if self.debug:
                response_text = "An error has occurred:\n\n" + format_exc()
            else:
                response_text = "An error has occurred.\n"

        response_text = "\r\n".join(response_text.splitlines()) + "\r\n"
        outp.write(response_text.encode("ascii", errors="ignore"))

    async def _handle_async_connection(
        self,
        inp: asyncio.StreamReader,
        outp: asyncio.StreamWriter,
        /,
    ) -> None:
        """Handle a new incoming asynchronous connection.

        :param inp: The stream reader for the connection.
        :param outp: The stream writer for the connection.
        """
        try:
            await self._handle_async_finger_connection(inp, outp)
        except Exception:
            logger.exception("The following exception has occurred:")

            if not self.debug:
                outp.write(b"An internal exception has occurred.\r\n")
            else:
                traceback = "\r\n  ".join(format_exc().splitlines())
                outp.write(
                    b"An internal exception has occurred:\r\n\r\n  "
                    + traceback.encode("ascii", errors="ignore")
                    + b"\r\n",
                )

            raise
        finally:
            try:
                outp.close()
                await outp.wait_closed()
            except Exception:
                logger.exception("Failed closing the connection:")

    async def _start_server_for_bind(self, bind: FingerServerBind, /) -> None:
        """Start an asynchronous server."""
        if isinstance(bind.address, IPv4Address):
            family, host, port = AF_INET, str(bind.address), bind.port
        elif isinstance(bind.address, IPv6Address):
            family, host, port = AF_INET6, str(bind.address), bind.port
        else:  # pragma: no cover
            raise NotImplementedError()

        try:
            server = await asyncio.start_server(
                self._handle_async_connection,
                host=host,
                port=port,
                family=family,
                reuse_address=True,
            )
        except OSError as exc:
            name = errorcode[exc.errno]

            if name == "EADDRINUSE":
                logger.error(
                    "Could not bind to [%s]:%d: address already in use.",
                    host,
                    port,
                )
            elif name == "EACCES":
                logger.error(
                    "Could not bind to [%s]:%d: port %d is a privileged "
                    + "port and process is unprivileged.",
                    host,
                    port,
                    port,
                )
            elif name == "EADDRNOTAVAIL":
                logger.error(
                    "Could not bind to [%s]:%d: %s is not "
                    + "available to bind.",
                    host,
                    port,
                    host,
                )
            else:
                raise
        else:
            logger.info("Starting pyfingerd on [%s]:%d.", host, port)

            try:
                await server.serve_forever()
            except asyncio.CancelledError:
                pass

            logger.info("Stopping pyfingerd on [%s]:%d", host, port)

    def prepare_auxiliary_coroutines(self, /) -> Iterable[Coroutine]:
        """Get the general coroutines for the server, excluding binds.

        This can be used by the child server to run coroutines aside the
        server coroutines, e.g. workers to update the state.

        By default, this method does not yield any auxiliary coroutine.

        :return: Coroutine iterator.
        """
        return
        yield

    def _prepare_coroutines(self, /) -> Iterable[Coroutine]:
        """Prepare coroutines to be run within the server process.

        This prepares one server coroutine per bind, and auxiliary coroutines
        that may be instantiated by :py:meth:`prepare_auxiliary_coroutines`.

        :return: The coroutine iterator.
        """
        for bind in self.binds:
            yield self._start_server_for_bind(bind)

        yield from self.prepare_auxiliary_coroutines()

    async def start_async(self, /) -> None:
        """Start the servers."""
        tasks: tuple[asyncio.Task, ...] = tuple(
            asyncio.create_task(co) for co in self._prepare_coroutines()
        )

        await asyncio.wait(tasks, return_when=asyncio.FIRST_COMPLETED)

        # If any task has set an exception, we try to catch it.
        exc = None
        for task in tasks:
            if exc is None:
                try:
                    exc = task.exception()
                except asyncio.exceptions.InvalidStateError:
                    exc = None

            task.cancel()

        if exc is not None:
            raise exc

    def start_sync(self, /) -> None:
        """Start the coroutines to run the server and background tasks."""
        try:
            asyncio.run(self.start_async())
        except KeyboardInterrupt:
            pass

    def start(self, /) -> None:
        """Start all underlying server processes and bind all ports."""
        if self._process is not None and self._process.is_alive():
            return

        process = Process(target=self.start_sync)
        process.start()
        self._process = process

    def stop(self, /) -> None:
        """Stop all underlying server processes and unbind all ports."""
        process = self._process
        if process is None or not process.is_alive():
            return

        process.kill()
        process.join()
        self._process = None

    def serve_forever(self, /) -> None:
        """Start all servers and serve in a synchronous fashion.

        It starts all servers :py:meth:`FingerServer.start`, waits for
        an interrupt signal, and stops all servers using
        :py:meth:`FingerServer.stop`.
        """
        if self._process is not None:
            self.start()

            try:
                while True:
                    pause()
            except KeyboardInterrupt:
                pass

            self.stop()
        else:
            # If the server hasn't been started on another process
            # using ``.start()``, we can just start is on this process.
            self.start_sync()

    def shutdown(self, /) -> None:
        """Shutdown the server, alias to `.stop()`."""
        self.stop()


class FingerServer(FingerBaseServer):
    """Finger server making use of an interface and a formatter."""

    hostname: Annotated[
        FingerHostname,
        AfterValidator(str.upper),
    ] = "LOCALHOST"
    """Hostname."""

    interface: Annotated[
        FingerInterface,
        Field(default_factory=FingerInterface),
    ]
    """Interface to use for querying users and sessions."""

    formatter: Annotated[
        FingerFormatter,
        Field(default_factory=FingerFormatter),
    ]
    """Formatter to use for formatting answers sent to clients."""

    def __init__(self, binds: str = "localhost:79", **kwargs) -> None:
        kwargs["binds"] = binds
        super().__init__(**kwargs)

    async def _cron_call(
        self,
        func: Callable[[], None],
        spec: croniter,
    ) -> None:
        """Call a function periodically using a cron specification.

        :param func: The function to call periodically.
        :param spec: The croniter specification to follow.
        """
        spec.set_current(datetime.now())

        while True:
            try:
                func()
            except SystemExit:
                return

            while True:
                seconds = (
                    spec.get_next(datetime) - datetime.now()
                ).total_seconds()
                if seconds >= 0:
                    break

            await asyncio.sleep(seconds)

    def prepare_auxiliary_coroutines(self) -> Iterable[Coroutine]:
        """Get the general coroutines for the server, excluding binds.

        We override this method to instanciate a cron per special
        method present in the interface, to allow e.g. updating the
        interface state regularly.

        :return: Coroutine iterator.
        """
        for key in dir(self.interface):
            member = getattr(self.interface, key)
            if not callable(member):
                continue

            try:
                spec = member.__cron__
            except AttributeError:
                continue

            if not isinstance(spec, croniter):
                continue

            yield self._cron_call(member, spec)

    async def handle_malformed_request(
        self,
        /,
        *,
        exc: MalformedRequestError,
        src: IPv4Address | IPv6Address,
    ) -> str:
        """Handle an invalid request.

        :param exc: Raised exception describing the request parsing error.
        :param src: Source from which the request has been emitted.
        :return: ASCII-compatible result.
        """
        error_logger.info(
            "%s made a bad request: %s in %r.",
            src,
            exc.msg,
            exc.line,
        )
        return self.formatter.format_query_error(self.hostname, exc.line)

    async def handle_request(
        self,
        request: FingerRequest,
        /,
        *,
        src: IPv4Address | IPv6Address,
    ) -> str:
        """Handle a valid request.

        :param request: Decoded request.
        :param src: Source from which the request has been emitted.
        :return: ASCII-compatible result.
        """
        if request.host is not None:
            if request.query:
                access_logger.info(
                    "%s requested transmitting user query for %r at %r.",
                    src,
                    request.query,
                    request.host,
                )
            else:
                access_logger.info(
                    "%s requested transmitting user query to %r.",
                    src,
                    request.host,
                )

            return self.interface.transmit_query(
                request.query,
                request.host,
                request.verbose,
            )

        if request.query:
            users = self.interface.search_users(request.query, None)
            access_logger.info(
                "%s requested user %r: found %s.",
                src,
                request.query,
                {0: "no user", 1: "1 user"}.get(
                    len(users),
                    f"{len(users)} users",
                ),
            )
        else:
            users = self.interface.search_users(None, True)
            access_logger.info(
                "%s requested connected users: found %s.",
                src,
                {0: "no user", 1: "1 user"}.get(
                    len(users),
                    f"{len(users)} users",
                ),
            )

        if request.query or request.verbose:
            return self.formatter.format_long(
                self.hostname,
                request.line,
                users,
            )

        return self.formatter.format_short(
            self.hostname,
            request.line,
            users,
        )
