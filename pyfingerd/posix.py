#!/usr/bin/env python3
# *****************************************************************************
# Copyright (C) 2017-2025 Thomas Touhey <thomas@touhey.fr>
# This file is part of the pyfingerd Python 3.x module, which is MIT-licensed.
# *****************************************************************************
"""Make use of the utmp/x file to read the user data.

This module can only load on POSIX compliant environments.
Importing it from other environments will result in an
:py:exc:`python:ImportError`.
"""

from __future__ import annotations

from collections import defaultdict
from datetime import datetime, timezone
from logging import getLogger
from multiprocessing import Lock
from os import stat
from pathlib import Path
from pwd import getpwall

import pyutmpx

from .core import FingerInterface, FingerSession, FingerUser


__all__ = ["FingerPOSIXInterface"]

logger = getLogger(__name__)


class FingerPOSIXInterface(FingerInterface):
    """Finger interface for POSIX-compliant systems.

    The method for gathering users and sessions on such systems
    is the following:

    #. Get the users in ``/etc/passwd``, and check which ones not
       to make appear through the presence of ``.nofinger``
       in their home directory.
    #. Get the last login times for all users to display them
       by default, through the lastlog database if available.
    #. Get the sessions in the utmp / utmpx database, and make
       them correspond to the related user.
    #. For each session, get the idle time by gathering the
       mtime of the device.
    """

    __slots__ = ("_data", "_ignoreduids", "_lastrefreshtime", "_lock")

    _data: list[FingerUser]
    _ignoreduids: set[int]
    _lastrefreshtime: datetime | None

    def __init__(self):
        self._data = []
        self._ignoreduids = set()
        self._lastrefreshtime = None
        self._lock = Lock()

    def search_users(
        self,
        query: str | None,
        active: bool | None,
    ) -> tuple[FingerUser, ...]:
        """Look for users on POSIX-compliant systems.

        :param query: The user query.
        :param active: Whether to look for active users or not.
        :return: The result.
        """
        self._lock.acquire()

        try:
            # Refresh the user list if required.
            if (
                self._lastrefreshtime is None
                or abs(
                    (
                        self._lastrefreshtime - datetime.utcnow()
                    ).total_seconds(),
                )
                >= 1
            ):
                lastlog_by_user_id: dict[int, datetime] = {}
                sessions_by_user_login: defaultdict[
                    str,
                    list[FingerSession],
                ] = defaultdict(list)

                try:
                    lastlog = pyutmpx.lastlog
                except AttributeError:
                    pass
                else:
                    for lle in lastlog:
                        lastlog_by_user_id[lle.uid] = lle.time

                try:
                    utmp = pyutmpx.utmp
                except AttributeError:
                    pass
                else:
                    for utmp_entry in utmp:
                        if utmp_entry.type != pyutmpx.USER_PROCESS:
                            continue

                        start: datetime = utmp_entry.time
                        if start.tzinfo is None:
                            start = start.replace(tzinfo=timezone.utc)

                        idle = datetime.utcnow()
                        if utmp_entry.line and not utmp_entry.line.startswith(
                            ":",
                        ):
                            dev_path = ("", "/dev/")[
                                utmp_entry.line[0] != "/"
                            ] + utmp_entry.line
                            idle = datetime.fromtimestamp(
                                stat(dev_path).st_atime,
                            )
                            if idle.tzinfo is None:
                                idle = idle.replace(tzinfo=timezone.utc)

                        sessions_by_user_login[utmp_entry.user].append(
                            FingerSession(
                                start=start,
                                idle=idle,
                                line=utmp_entry.line,
                                host=utmp_entry.host,
                            ),
                        )

                users: list[FingerUser] = []
                for pw_entry in getpwall():
                    if pw_entry.pw_uid in self._ignoreduids:
                        continue

                    try:
                        if (Path(pw_entry.pw_dir) / ".nofinger").exists():
                            continue
                    except (PermissionError, NotADirectoryError):
                        logger.warning(
                            "Could not access .nofinger for user %s, ignoring",
                            pw_entry.pw_name,
                            exc_info=True,
                        )
                        self._ignoreduids.add(pw_entry.pw_uid)
                        continue

                    office = None
                    gecos = pw_entry.pw_gecos.split(",")
                    if len(gecos) >= 2:
                        office = gecos[1]

                    plan = None
                    try:
                        with open(Path(pw_entry.pw_dir, ".plan")) as plan_file:
                            plan = plan_file.read()
                    except FileNotFoundError:
                        pass
                    except (PermissionError, NotADirectoryError):
                        logger.warning(
                            "Could not access .plan for user %s, ignoring",
                            pw_entry.pw_name,
                            exc_info=True,
                        )
                        self._ignoreduids.add(pw_entry.pw_uid)
                        continue

                    # We may have a lastlog entry
                    sessions = sessions_by_user_login.get(pw_entry.pw_name, [])
                    last_login = lastlog_by_user_id.get(pw_entry.pw_uid)
                    if sessions:
                        session_times = [session.start for session in sessions]
                        if last_login is not None:
                            session_times.append(last_login)

                        last_login = max(session_times)

                    users.append(
                        FingerUser(
                            login=pw_entry.pw_name,
                            name=gecos[0],
                            shell=pw_entry.pw_shell,  # noqa: S604
                            home=pw_entry.pw_dir,
                            office=office,
                            plan=plan,
                            last_login=last_login,
                            sessions=sessions,
                        ),
                    )

                # We're done refreshing!
                self._data = users
                self._lastrefreshtime = datetime.utcnow()

            return tuple(
                user
                for user in self._data
                if (
                    (
                        query is None
                        or (user.login is not None and query in user.login)
                    )
                    and (active is None or active == bool(user.sessions))
                )
            )
        finally:
            self._lock.release()
