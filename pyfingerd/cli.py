#!/usr/bin/env python3
# *****************************************************************************
# Copyright (C) 2021-2025 Thomas Touhey <thomas@touhey.fr>
# This file is part of the pyfingerd project, which is MIT-licensed.
# *****************************************************************************
"""pyfingerd CLI interface."""

from __future__ import annotations

from datetime import datetime
from logging import getLogger
from os import EX_IOERR, EX_OSFILE, EX_USAGE
from platform import python_implementation, python_version
from sys import exit as sys_exit

import click
import coloredlogs

from . import __version__ as pyfingerd_version
from .core import FingerInterface
from .fiction import FingerScenario, FingerScenarioInterface
from .native import FingerNativeInterface
from .server import FingerServer


__all__ = ["cli"]

logger = getLogger(__name__)


@click.command(context_settings={"help_option_names": ["-h", "--help"]})
@click.version_option(
    version=pyfingerd_version,
    message=(
        f"pyfingerd version {pyfingerd_version}, "
        + f"running on {python_implementation()} {python_version()}"
    ),
)
@click.option(
    "-b",
    "--binds",
    default="localhost:79",
    envvar=("BIND", "BINDS"),
    show_default=True,
    help="Addresses and ports on which to listen to, comma-separated.",
)
@click.option(
    "-H",
    "--hostname",
    default="LOCALHOST",
    envvar=("FINGER_HOST",),
    show_default=True,
    help="Hostname to display to finger clients.",
)
@click.option(
    "-t",
    "--type",
    "type_",
    default=None,
    envvar=("FINGER_TYPE",),
    show_default=True,
    help="Interface type for gathering user and session data.",
)
@click.option(
    "-s",
    "--scenario",
    default=None,
    envvar=("FINGER_SCENARIO", "FINGER_ACTIONS"),
    help="Path to the scenario, if the selected type is 'scenario'.",
)
@click.option(
    "-S",
    "--start",
    "scenario_start",
    type=click.DateTime(),
    default=datetime.now().strftime("%Y-%m-%dT%H:%M:%S"),
    envvar=("FINGER_START",),
    help=(
        "Date and time at which the scenario starts or has started "
        + "as an ISO date, if the selected type is 'scenario'."
    ),
)
@click.option(
    "-l",
    "--loglevel",
    "log_level",
    default="info",
    envvar=("FINGER_LOGLEVEL",),
    help="Log level for the displayed messages.",
)
def cli(
    binds: str,
    hostname: str,
    type_: str,
    scenario: str | None,
    scenario_start: datetime,
    log_level: str,
) -> None:
    """Start a finger (RFC 1288) server.

    Find out more at <https://pyfingerd.org/>.
    """
    coloredlogs.install(
        fmt="\r%(asctime)s.%(msecs)03d %(levelname)s %(message)s",
        datefmt="%d/%m/%Y %H:%M:%S",
        level=log_level.upper(),
    )

    hostname = hostname.upper()

    if type_ is None:
        if scenario is not None:
            type_ = "scenario"
        else:
            type_ = "native"

    type_ = type_.casefold()
    if scenario is not None and type_ not in ("actions", "scenario"):
        logger.warning(
            "Since the type isn't 'scenario', the provided scenario "
            + "path will be ignored.",
        )

    if type_ == "native":
        iface = FingerNativeInterface()
    elif type_ in ("actions", "scenario"):
        if scenario is None:
            logger.error(
                "Scenario interface selected, but the scenario has not "
                + "been provided; please do so.",
            )
            sys_exit(EX_USAGE)

        try:
            fic = FingerScenario.load(scenario)
            iface = FingerScenarioInterface(fic, scenario_start)
        except (FileNotFoundError, PermissionError):
            logger.error("Error opening the scenario:", exc_info=True)
            sys_exit(EX_OSFILE)
        except ValueError as exc:
            message = str(exc)
            logger.error("Error loading the scenario:")
            logger.error("%s%s.", message[0].upper(), message[1:])
            sys_exit(EX_OSFILE)
    else:
        if type_ != "dummy":
            logger.warning(
                "Unknown interface type %r, falling back on dummy",
                type_,
            )

        iface = FingerInterface()

    try:
        server = FingerServer(binds, hostname=hostname, interface=iface)
    except ValueError:
        logger.exception("Server could not be run:")
        sys_exit(EX_IOERR)

    server.serve_forever()
