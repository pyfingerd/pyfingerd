#!/usr/bin/env python3
# *****************************************************************************
# Copyright (C) 2021-2025 Thomas Touhey <thomas@touhey.fr>
# This file is part of the pyfingerd Python 3.x module, which is MIT-licensed.
# *****************************************************************************
"""Utilities for the pyfingerd module."""

from __future__ import annotations

from datetime import timedelta
from logging import getLogger
import re
from string import ascii_letters, digits
from typing import ClassVar


__all__ = [
    "UnchangedType",
    "Unchanged",
    "access_logger",
    "error_logger",
    "format_delta",
    "parse_delta",
]

ALLOWED_FINGER_CHARS = (
    "\t !\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~" + ascii_letters + digits
)

_DELTA0_PATTERN = re.compile(r"(-?)(([0-9]+[a-z]+)+)")
_DELTA1_PATTERN = re.compile(r"([0-9]+)([a-z]+)")

access_logger = getLogger("pyfingerd.access")
error_logger = getLogger("pyfingerd.error")


class UnchangedType:
    """Special constant class for representing 'no changes required'."""

    __slots__ = ()

    __unchanged_value__: ClassVar[UnchangedType]

    def __new__(cls: type[UnchangedType], /, *args, **kwargs):
        """Construct the class."""
        # We want the same instance to be returned always when required.
        try:
            return cls.__unchanged_value__
        except AttributeError:
            value = super().__new__(cls, *args, **kwargs)
            cls.__unchanged_value__ = value
            return value

    def __repr__(self, /):
        return "Unchanged"


Unchanged = UnchangedType()


def parse_delta(raw: str, /) -> timedelta:
    """Parse a delta string as found in the configuration files.

    :param raw: Formatted delta to decode.
    :return: Parsed delta.
    :raises ValueError: The provided delta was incorrectly formatted.
    """
    delta = timedelta()

    match = _DELTA0_PATTERN.fullmatch(raw)
    if match is None:
        raise ValueError("Invalid time format")

    sign, elements, _ = match.groups()
    sign = (1, -1)[len(sign)]
    for res in _DELTA1_PATTERN.finditer(elements):
        num, typ = res.groups()
        num = int(num)

        if typ == "w":
            delta += timedelta(weeks=sign * num)
        elif typ in "jd":
            delta += timedelta(days=sign * num)
        elif typ == "h":
            delta += timedelta(hours=sign * num)
        elif typ == "m":
            delta += timedelta(minutes=sign * num)
        elif typ == "s":
            delta += timedelta(seconds=sign * num)
        else:
            raise ValueError(f"Invalid time format component: {res[0]}")

    return delta


def format_delta(delta: timedelta, /) -> str:
    """Create a delta string.

    :param delta: Delta to format.
    :return: Formatted delta.
    """
    sls = zip(
        (
            timedelta(days=7),
            timedelta(days=1),
            timedelta(seconds=3600),
            timedelta(seconds=60),
        ),
        "wdhm",
    )

    if delta >= timedelta():
        d = ""

        for span, letter in sls:
            n = delta // span
            if n:
                d += f"{n}{letter}"
                delta -= span * n

        s = delta.seconds
        if not d or s:
            d += f"{s}s"
    else:
        d = "-"

        for span, letter in sls:
            n = -delta // span
            if n:
                d += f"{n}{letter}"
                delta += span * n

        s = (-delta).seconds
        if s:
            d += f"{s}s"

    return d


def make_delta(value: str | int | float | timedelta | None) -> timedelta:
    """Make a delta from a raw value.

    :param value: Value to make a delta from.
    :return: Obtained delta.
    :raises ValueError: No delta could be decoded from the provided value.
    """
    if value is None:
        raise ValueError("must not be None")

    if isinstance(value, timedelta):
        return value

    try:
        value = int(value)
    except (TypeError, ValueError) as exc:
        if isinstance(value, str):
            new_value = parse_delta(value)
            if new_value is not None:
                return new_value

            raise ValueError(f"Invalid time delta: {value!r}") from exc

        raise TypeError(f"Unknown type {type(value).__name__}") from exc

    return timedelta(seconds=value)


def decode_finger_text(data: bytes, /) -> str:
    """Decode ASCII data for Finger data.

    This function is in charge of the following constraint::

        By default, this program SHOULD filter any unprintable data, leaving
        only printable 7-bit characters (ASCII 32 through ASCII 126),
        tabs (ASCII 9) and CRLFs.

    :param data: Raw data to decode a finger request from.
    :return: Raw string.
    """
    return "".join(
        char
        for char in data.decode("ascii", errors="ignore")
        if char in ALLOWED_FINGER_CHARS
    )
