#!/usr/bin/make -f
include Makefile.msg

help:
	$(call amsg,Available targets are:)
	$(call amsg,)
	$(call amsg,- install)
	$(call amsg,- lint)
	$(call amsg,- test)

install:
	$(call bmsg,Installing poetry and dependencies.)
	$(call qcmd,pip install -U poetry)
	$(call qcmd,poetry install)

lint:
	$(call bcmd,pre-commit,run,-poetry run pre-commit run --all-files)

flake8:
	$(call bcmd,flake8,,-poetry run pre-commit run flake8 --all-files)

mypy:
	$(call bcmd,mypy,.,-poetry run pre-commit run mypy --all-files)

build:
	$(call bcmd,poetry build,.,poetry build)

run:
	@poetry run python -m pyfingerd -b 'localhost:3999'

redirect-localhost-ports:
	iptables -t nat -A OUTPUT -p tcp -s 127.0.0.1 -d 127.0.0.1 \
		--dport 79 -j DNAT --to 127.0.0.1:3999
	ip6tables -t nat -A OUTPUT -p tcp -s ::1 -d ::1 \
		--dport 79 -j DNAT --to '[::1]:3999'

test:
	$(call qcmd,rm -rf htmlcov)
	$(call bcmd,pytest,--cov, \
		poetry run pytest --cov=pyfingerd --cov-report html \
		--cov-report term $(O) $(SPECIFIC_TESTS))
	$(call bmsg,HTML coverage is available under the following directory:)
	$(call bmsg,file://$(realpath .)/htmlcov/index.html)

clean:
	$(call rmsg,Cleaning build and cache directories.)
	$(call qcmd,rm -rf build .coverage htmlcov .mypy_cache .pytest_cache)

.PHONY: help install lint flake8 pyright mypy build test clean
.PHONY: check-exports
