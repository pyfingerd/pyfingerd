#!/usr/bin/env python3
# *****************************************************************************
# Copyright (C) 2021 Thomas Touhey <thomas@touhey.fr>
# This file is part of the pyfingerd project, which is MIT-licensed.
# *****************************************************************************
"""Tests for the pyfingerd server."""

from __future__ import annotations

import pytest

from pyfingerd.fiction import (
    EndingType,
    FingerScenario,
    FingerUserCreationAction,
    FingerUserDeletionAction,
    FingerUserEditionAction,
    FingerUserLoginAction,
    FingerUserLogoutAction,
)


def test_edit_without_create_user():
    """Test that we cannot edit a user that hasn't been created."""
    scenario = FingerScenario(
        ending_type=EndingType.FREEZE,
        duration="1m",
    )

    scenario.add(
        FingerUserEditionAction(
            login="user",
            shell="/bin/sh",  # NOQA
        ),
        "0s",
    )

    with pytest.raises(ValueError, match=r"trying to edit user"):
        scenario.verify()


def test_delete_without_create_user():
    """Test that we cannot delete a user that hasn't been created."""
    scenario = FingerScenario(
        ending_type=EndingType.FREEZE,
        duration="1m",
    )

    scenario.add(
        FingerUserDeletionAction(
            login="user",
        ),
        "0s",
    )

    with pytest.raises(ValueError, match=r"trying to delete user"):
        scenario.verify()


def test_edit_after_delete_user():
    """Test that we cannot edit a user that has been deleted."""
    scenario = FingerScenario(
        ending_type=EndingType.FREEZE,
        duration="1m",
    )
    scenario.add(
        FingerUserCreationAction(
            login="user",
            shell="/bin/sh",  # NOQA
        ),
        "0s",
    )
    scenario.add(
        FingerUserEditionAction(
            login="user",
            name="John Doe",
        ),
        "10s",
    )
    scenario.add(
        FingerUserDeletionAction(
            login="user",
        ),
        "5s",
    )

    with pytest.raises(ValueError, match=r"trying to edit user"):
        scenario.verify()


def test_login_without_creation():
    """Test that we cannot log a user in without creating it beforehand."""
    scenario = FingerScenario(
        ending_type=EndingType.FREEZE,
        duration="1m",
    )
    scenario.add(
        FingerUserLoginAction(
            login="user",
        ),
        "0s",
    )

    with pytest.raises(ValueError, match=r"trying to log in as"):
        scenario.verify()


def test_logout_without_creation():
    """Test that we cannot log a user out without creating it beforehand."""
    scenario = FingerScenario(
        ending_type=EndingType.FREEZE,
        duration="1m",
    )
    scenario.add(
        FingerUserLogoutAction(login="user"),
        "0s",
    )

    with pytest.raises(
        ValueError,
        match=r"trying to delete session of non-existing user",
    ):
        scenario.verify()


def test_logout_without_login():
    """Test that we cannot log a user out without logging it in beforehand."""
    scenario = FingerScenario(
        ending_type=EndingType.FREEZE,
        duration="1m",
    )
    scenario.add(
        FingerUserCreationAction(
            login="user",
            shell="/bin/sh",  # NOQA
        ),
        "0s",
    )
    scenario.add(
        FingerUserLogoutAction(
            login="user",
        ),
        "1s",
    )

    with pytest.raises(
        ValueError,
        match=r"trying to delete unprovisioned unnamed session",
    ):
        scenario.verify()
