#!/usr/bin/env python3
# *****************************************************************************
# Copyright (C) 2021 Thomas Touhey <thomas@touhey.fr>
# This file is part of the pyfingerd project, which is MIT-licensed.
# *****************************************************************************
"""Tests for the pyfingerd server."""

from __future__ import annotations

from datetime import timedelta
from logging import getLogger
import socket
from time import sleep

import pytest

from pyfingerd.core import FingerFormatter, FingerUser
from pyfingerd.fiction import (
    FingerScenario,
    FingerScenarioInterface,
    FingerUserCreationAction,
    FingerUserLoginAction,
    FingerUserLogoutAction,
)
from pyfingerd.server import FingerServer


logger = getLogger(__name__)


@pytest.fixture()
def host():
    """Return the host."""
    return "127.0.0.1"


@pytest.fixture()
def port(host):
    """Get a free port for the given host."""
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    try:
        sock.bind((host, 0))
        return sock.getsockname()[1]
    finally:
        sock.close()


@pytest.fixture()
def hostname():
    """Test with a different host name."""
    return "EXAMPLE.ORG"


@pytest.fixture(autouse=True)
def fingerserver(host, port, hostname):
    """Start a finger server.

    A fixture starting a finger server on ``localhost:3099``
    and stopping it after the test.
    """
    scenario = FingerScenario(
        ending_type="freeze",
        duration=timedelta(seconds=5),
    )
    scenario.add(
        FingerUserCreationAction(
            login="john",
            name="John Doe",
            home="/home/john",
            shell="/bin/bash",  # NOQA
            office="84.6",
        ),
        timedelta(seconds=-5),
    )
    scenario.add(
        FingerUserLoginAction(
            login="john",
            line="tty1",
        ),
        timedelta(seconds=0),
    )
    scenario.add(
        FingerUserLogoutAction(
            login="john",
        ),
        timedelta(seconds=1),
    )

    class TestFormatter(FingerFormatter):
        """Test formatter, uncomplicated to test."""

        def _format_users(self, users: list[FingerUser]) -> str:
            result = f"{len(users)}\n"
            for user in users:
                result += (
                    f"{user.login}|{user.name}|{user.home}|"
                    + f'{user.shell}|{user.office or ""}|'
                    + f"{len(user.sessions)}\n"
                )
                for session in user.sessions:
                    result += f'{session.line or ""}|{session.host or ""}\n'

            return result

        def format_query_error(self, hostname, raw_query):
            return f"{hostname}\n{raw_query}\nerror\n"

        def format_short(self, hostname, raw_query, users):
            return f"{hostname}\n{raw_query}\nshort\n" + self._format_users(
                users,
            )

        def format_long(self, hostname, raw_query, users):
            return f"{hostname}\n{raw_query}\nlong\n" + self._format_users(
                users,
            )

    server = FingerServer(
        f"{host}:{port}",
        hostname=hostname,
        interface=FingerScenarioInterface(scenario),
        formatter=TestFormatter(),
        debug=True,
    )
    server.start()

    sleep(0.1)
    yield

    server.stop()


@pytest.fixture()
def send_command(host, port, hostname):
    """Get the command sending function."""

    def send_command(command):
        """Send the command in a separate connection to the server."""
        conn = socket.create_connection((host, port), timeout=1)
        conn.send(command.encode("ascii") + b"\r\n")

        result = tuple(
            conn.recv(4096).decode("ascii").rstrip("\r\n").split("\r\n"),
        )

        logger.info("Received the following:\n\n%s", "\n".join(result))

        assert result[:2] == (hostname, command)
        return result[2:]

    return send_command


# ---
# Tests.
# ---


def test_no_user_list(send_command):
    """Test if an unknown user returns an empty result."""
    assert send_command("user") == ("long", "0")


def test_existing_user_list(send_command):
    """Test the user list before and after the cron is executed."""
    assert send_command("") == (
        "short",
        "1",
        "john|John Doe|/home/john|/bin/bash|84.6|1",
        "tty1|",
    )

    sleep(2)

    assert send_command("") == ("short", "0")
