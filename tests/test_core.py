#!/usr/bin/env python3
# *****************************************************************************
# Copyright (C) 2023 Thomas Touhey <thomas@touhey.fr>
# This file is part of the pyfingerd project, which is MIT-licensed.
# *****************************************************************************
"""Tests for the pyfingerd core classes."""

from __future__ import annotations

from datetime import datetime, timezone

from pyfingerd.core import FingerFormatter, FingerSession, FingerUser


# ---
# Data classes.
# ---


def test_session_with_start():
    """Instantiate a session with a start and test the data."""
    session = FingerSession(start=datetime.min)
    assert session.start.replace(tzinfo=None) == datetime.min
    assert session.start.tzinfo is timezone.utc
    assert session.idle == session.start
    assert session.line is None
    assert session.host is None


def test_session_with_start_with_idle_before():
    """Instantiate a session and force idle time coerscing."""
    session = FingerSession(
        start=datetime(10, 1, 1),
        idle=datetime(5, 1, 1),
    )
    assert session.start.replace(tzinfo=None) == datetime(10, 1, 1)
    assert session.start.tzinfo is timezone.utc
    assert session.idle == session.start
    assert session.line is None
    assert session.host is None


def test_user_tz_naive_last_login():
    """Test that UTC is automatically added to the last login datetime."""
    user = FingerUser(last_login=datetime.min)
    assert user.last_login.replace(tzinfo=None) == datetime.min
    assert user.last_login.tzinfo is timezone.utc


# ---
# Formatter tests.
# ---


def test_formatter_header():
    """Test formatting the header using a host and a command."""
    obj = FingerFormatter()
    assert obj.format_query_error("OHNO.EXAMPLE", "abc /") == "\r\n".join(
        (
            "Site: OHNO.EXAMPLE",
            "You have made a mistake in your query!",
            "",
        ),
    )
