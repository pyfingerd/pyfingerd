.. _pyfingerd:

``pyfingerd`` command-line reference
====================================

``pyfingerd`` (or ``python -m pyfingerd``) is the command-line used to run
the server.

When run through CLI, pyfingerd exposes a number of configuration options using
its own code. Each configuration option is configurable through three
cumulative means:

* A UNIX-style command-line option.
* An environment variable set directly.
* An environment variable set through a dotenv file (``.env``).

When a configuration option is given through multiple means, the resolution
order of such the value is the one given above.

Available options are:

``-b``, ``--binds`` (``BIND``)
    List of hosts and ports to bind to.

    This option must be defined one way or another, and must contain the list
    of ports to bind separated with commas (``,``). Each port must have one of
    the following formats:

    ``example.com``
        Bind on all addresses that ``example.com`` resolve as (IPv4 and IPv6),
        port 79.

    ``example.com:1234``
        Bind on all addresses that ``example.com`` resolve as (IPv4 and IPv6),
        port 1234.

    ``1.2.3.4`` or ``[1.2.3.4]``
        Bind on ``1.2.3.4`` (IPv4), port 79.

    ``1.2.3.4:1234`` or ``[1.2.3.4]:1234``
        Bind on ``1.2.3.4`` (IPv4), port 1234.

    ``::1:2:3:4`` or ``[::1:2:3:4]``
        Bind on ``::1:2:3:4`` (IPv6) port 79.

    ``[::1:2:3:4]:1234``
        Bind on ``::1:2:3:4`` (IPv6) port 1234.

    Here are some examples:

    .. code-block:: bash

        # On modern platforms, binds on 127.0.0.1:79 (IPv4)
        # and [::1]:79 (IPv6).
        BIND=localhost

        # Binds on 1.2.3.4:3331 and [2001:41d0:302:2200::3b2]:79.
        BIND="1.2.3.4:3331,[2001:41d0:302:2200::3b2]:79"
        BIND="1.2.3.4:3331,2001:41d0:302:2200::3b2"

``-H``, ``--hostname`` (``FINGER_HOST``)
    Hostname for the server to identify itself when answering queries.

    This option is set to ``LOCALHOST`` by default.

.. _cli-type:

``-t``, ``--type`` (``FINGER_TYPE``)
    Type of interface for the finger server to query users and sessions.

    The possible values for this option are the following:

    ``DUMMY``
        There is no data (no users, no sessions).

    ``NATIVE``
        The data is gathered from the system.

    ``SCENARIO``
        The data is gathered from a scenario (see :ref:`cli-scenario` below).

    By default, the interface type is ``NATIVE``. If an invalid server type is
    provided, the server is considered a dummy server.

.. _cli-scenario:

``-s``, ``--scenario`` (``FINGER_ACTIONS``)
    Path to the scenario, in TOML format.

    This option is only used when setting the interface type to ``SCENARIO``;
    see :ref:`cli-type` for more information.

    The destination file must be a TOML file representing a scenario;
    see :ref:`scenario-files` for more information.
