|pyfingerd| pyfingerd |version|
===============================

finger is both a protocol and a utility to get the information and status
from a user on a distant machine. It was standardized in `RFC 742`_
in 1977, then in `RFC 1288`_ in 1991.

pyfingerd is an implementation for this protocol, which allows you to:

* Run a finger server using system information on Linux and \*BSD systems.
* Run a finger server using a scenario for animating alternate reality games
  or confusing potential attackers.
* Implement your own finger server in Python using sensible defaults.

For more information, consult the following links:

* `Project repository at Gitlab.com`_;
* `PyPI package detail`_;
* `Issue tracker`_;
* `Pending contributions`_.

This documentation is organized using `Diátaxis`_' structure.

How-to guides
-------------

These sections provide guides, i.e. recipes, targeted towards various actors.
They guide you through the steps involved in addressing key problems and
use-cases.

.. toctree::
    :maxdepth: 3

    guides
    admin-guides

Discussion topics
-----------------

These sections discuss key topics and concepts at a fairly high level, and
provide useful background information and explanation.

.. toctree::
    :maxdepth: 3

    topics

References
----------

These sections provide technical reference for APIs and other aspects of
pyfingerd's machinery. They go into detail, and therefore, assume you have a
basic understanding of key concepts.

.. toctree::
    :maxdepth: 3

    cli
    code

.. |pyfingerd| image:: _assets/pyfingerd.png

.. _RFC 742: https://datatracker.ietf.org/doc/html/rfc742
.. _RFC 1288: https://datatracker.ietf.org/doc/html/rfc1288
.. _Project repository at Gitlab.com: https://gitlab.com/pyfingerd/pyfingerd
.. _PyPI package detail: https://pypi.org/project/pyfingerd/
.. _Issue tracker: https://gitlab.com/pyfingerd/pyfingerd/-/issues
.. _Pending contributions:
    https://gitlab.com/pyfingerd/pyfingerd/-/merge_requests
.. _Diátaxis: https://diataxis.fr/
