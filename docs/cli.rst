.. _cli:

Command line reference
======================

The module includes some command-line utilities, for which the references are
available in the following sections.

.. toctree::
    :maxdepth: 2

    cli/pyfingerd
