Discussion topics
=================

fingerd has a number of concepts necessary for a full understanding of its
conception and an efficient use of it. You can find these concepts and
discussion topics in the following sections.

.. toctree::

    topics/structure
    topics/fictional-interfaces
    topics/scenarios
