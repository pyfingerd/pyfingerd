# Copyright(C) 2023 Thomas Touhey
# All rights reserved.

from __future__ import annotations

from typing import TYPE_CHECKING, Any


if TYPE_CHECKING:
    from sphinx.application import Sphinx


def remove_first_line_in_module_docstring(
    app: Sphinx,
    what: str,
    name: str,
    obj: Any,
    options: Any,
    lines: list[str],
) -> None:
    """Remove the first line (and empty line) of the docstring if is a module.

    See `autodoc-process-docstring`_ for more information regarding the
    parameters and

    .. _autodoc-process-docstring:
        https://www.sphinx-doc.org/en/master/usage/extensions/autodoc.html
        #event-autodoc-process-docstring
    """
    if what != "module" or not lines:
        return

    for i in range(1, len(lines)):
        if not lines[i]:
            lines[: i + 1] = []
            return

    lines[:] = []


def setup(app: Sphinx) -> None:
    """Set up the extension.

    :param app: The Sphinx application to set up the extension for.
    """
    app.connect(
        "autodoc-process-docstring",
        remove_first_line_in_module_docstring,
    )
