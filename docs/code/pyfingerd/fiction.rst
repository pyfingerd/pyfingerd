``pyfingerd.fiction`` -- Fictional interfaces definitions
=========================================================

.. automodule:: pyfingerd.fiction
    :members:
    :undoc-members:
    :show-inheritance:
