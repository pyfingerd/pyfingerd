``pyfingerd.posix`` -- POSIX compliant interfaces
=================================================

.. py:module:: pyfingerd.posix

This module can only load on POSIX compliant environments.
Importing it from other environments will result in an
:py:exc:`python:ImportError`.

.. py:class:: FingerPOSIXInterface

    Bases: :py:class:`pyfingerd.core.FingerInterface`

    Finger interface for POSIX-compliant systems.

    The method for gathering users and sessions on such systems
    is the following:

    #. Get the users in ``/etc/passwd``, and check which ones not
       to make appear through the presence of ``.nofinger``
       in their home directory.
    #. Get the last login times for all users to display them
       by default, through the lastlog database if available.
    #. Get the sessions in the utmp / utmpx database, and make
       them correspond to the related user.
    #. For each session, get the idle time by gathering the
       mtime of the device.
