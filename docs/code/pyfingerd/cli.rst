``pyfingerd.cli`` -- Command-line application definition
========================================================

.. automodule:: pyfingerd.cli
    :members:
    :undoc-members:
    :show-inheritance:

    .. py:data:: cli
        :type: click:click.Command

        Command-line application to run the server.

        This command is run when you call ``python -m pyfingerd``.
