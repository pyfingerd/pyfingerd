``pyfingerd.errors`` -- Error definitions
=========================================

.. automodule:: pyfingerd.errors
    :members:
    :undoc-members:
    :show-inheritance:
