``pyfingerd.core`` -- Core API definition
=========================================

.. automodule:: pyfingerd.core
    :members:
    :undoc-members:
    :show-inheritance:
