``pyfingerd.native`` -- Native interfaces definition
====================================================

.. py:module:: pyfingerd.native

.. autoclass:: FingerNoNativeFoundInterface
    :show-inheritance:
    :members:

.. py:class:: FingerNativeInterface

    Bases: :py:class:`pyfingerd.core.FingerInterface`

    Native interface, always available whatever the platform is.
